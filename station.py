#!/usr/bin/python

import logging
import pickle
from datetime import datetime
import os.path
import statistics
import railclient
import database

LDB_TOKEN="51724cb2-bf0d-464d-a195-66fb5ef08757"
WSDL = 'http://lite.realtime.nationalrail.co.uk/OpenLDBWS/wsdl.aspx?ver=2021-11-01'

FILENAME = "./data/{station_code}.pickle"
SECONDS_IN_DAY = 60 * 60 * 24


class Station:
    def __init__(self, station_code='URM'):
        logging.info("Initialising %s", station_code)
        self.station_code = station_code
        self.station_name = None
        self.start_time = datetime.now()

        self.cancelled_count = 0
        self.on_time = 0
        self.departure_delays = []
        self.previous_trains = {}
        self.restore()

        database.create_connection()
        self.dump()


    def clear(self):
        self.cancelled_count = 0
        self.on_time = 0
        self.departure_delays.clear()
        total_trains = self.cancelled_count+len(self.departure_delays)+self.on_time
        logging.warning("Cleared, %s: %d trains total remaining",self.station_code, total_trains)
        logging.warning("\t%s: %d trains await departure",self.station_code, len(self.previous_trains))


    def restore(self):
        filename = FILENAME.format(station_code=self.station_code)
        if os.path.isfile(filename):
            with open(filename, 'rb') as file:
                restored = pickle.load(file)
                self.station_name = None
                self.start_time = restored.start_time
                self.cancelled_count = restored.cancelled_count
                self.on_time = restored.on_time
                self.departure_delays = restored.departure_delays

            total_trains = self.cancelled_count+len(self.departure_delays)+self.on_time
            logging.warning("Starting from restore - %s: %d trains restored",
                            self.station_code, total_trains)

        else:
            logging.warning("Starting from clean - no saved state for %s", self.station_code)


    def find_departed_trains(self):
        if self.station_name is None:
            self.station_name = railclient.get_location_name(self.station_code)
            database.save_station_name(self.station_code, self.station_name)

        new_services = {}
        current_services = railclient.get_departure_board(self.station_code)
        if current_services is not None:
            for service in current_services:
                if service.serviceID in self.previous_trains:
                    self.previous_trains.pop(service.serviceID, None)
                new_services[service.serviceID] = {'scheduled': service['std'], 'estimated': service['etd']}
                if service['etd'] == 'Delayed':
                    logging.warning(f"Service with potential delay and no time {service['std']}")
                    logging.debug(service)

            self.calculate_departed_services_reliability(self.previous_trains)
            self.previous_trains = new_services


    def dump(self):
        logging.debug("Class information")
        logging.debug("Station code: %s", self.station_code)
        logging.debug("Station name: %s", self.station_name)
        logging.debug(self.start_time)

        logging.debug("On-time count:   %d", self.on_time)
        logging.debug("Cancelled count: %d", self.cancelled_count)
        logging.debug(self.departure_delays)


    def print_stats(self):
        total_trains = self.cancelled_count + len(self.departure_delays) + self.on_time
        logging.info('\n%d trains @ %s since: %s', total_trains, self.station_name, self.start_time.strftime("%H:%M on %d/%m/%Y"))
        if total_trains > 0:
            logging.info('\t%d trains on time (%.0f%%)', self.on_time, self.on_time/total_trains*100)
            logging.info('\t%d trains cancelled (%.0f%%)', self.cancelled_count, self.cancelled_count/total_trains*100)
            logging.info('\t%d trains delayed (%.0f%%)', len(self.departure_delays), len(self.departure_delays)/total_trains*100)
            if len(self.departure_delays) > 0:
                logging.info('\tAverage delay: %.1f minutes', statistics.mean(self.departure_delays))
                longer_delays = list(filter(lambda num: num > railclient.MINOR_DELAY_TIME, self.departure_delays))
                if len(longer_delays) > 0:
                    logging.info('\t\tAverage delay above %d mins: %.0f%% minutes, %d trains',
                                 railclient.MINOR_DELAY_TIME, statistics.mean(longer_delays), len(longer_delays))
                    logging.info('\t\tReliability w/o minor delay: %.0f%%',
                                 (total_trains-len(longer_delays)-self.cancelled_count)/total_trains*100)

                    major_delays = list(filter(lambda num: num >= railclient.MAJOR_DELAY_TIME, self.departure_delays))
                    if len(major_delays) > 0:
                        logging.info('\t\tAverage delay above %d mins: %.0f%% minutes, %d trains',
                                     railclient.MAJOR_DELAY_TIME, statistics.mean(major_delays), len(major_delays))
                        logging.info('\t\tMajor delays: %.0f%%', len(major_delays)/total_trains*100)

        else:
            logging.warning("No trains processed yet @ %s", self.station_code)


    def save_stats(self):
        print ("Saving")
        filename = FILENAME.format(station_code=self.station_code)
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

        trains_delayed_mean = 0
        longer_delays_mean = 0
        number_trains_longer_delays = 0
        major_delays_mean = 0
        number_trains_major_delays = 0

        if len(self.departure_delays) > 0:
            print ("Calculating delays")
            trains_delayed_mean = statistics.mean(self.departure_delays)
            longer_delays = list(filter(lambda num: num > railclient.MINOR_DELAY_TIME, self.departure_delays))
            if len(longer_delays) > 0:
                longer_delays_mean = statistics.mean(longer_delays)
                number_trains_longer_delays = len(longer_delays)
                major_delays = list(filter(lambda num: num >= railclient.MAJOR_DELAY_TIME, self.departure_delays))

                if len(major_delays) > 0:
                    major_delays_mean = statistics.mean(major_delays)
                    number_trains_major_delays = len(major_delays)
        else:
            print ("No delays to calculate")

        print ("On time:                     " + str(self.on_time))
        print ("Cancelled:                   " + str(self.cancelled_count))
        print ("departure_delays:            " + str(len(self.departure_delays)))
        print ("trains_delayed_mean:         " + str(trains_delayed_mean))
        print ("number_trains_longer_delays: " + str(number_trains_longer_delays))
        print ("longer_delays_mean:          " + str(longer_delays_mean))
        print ("number_trains_major_delays:  " + str(number_trains_major_delays))
        print ("major_delays_mean:           " + str(major_delays_mean))


        database.write_record(self.station_code, self.on_time, self.cancelled_count,
                              len(self.departure_delays), trains_delayed_mean,
                              number_trains_longer_delays, longer_delays_mean,
                              number_trains_major_delays, major_delays_mean)

        print ("Save complete")

    def calculate_departed_services_reliability(self, services):
        count = 0
        for service in services:
            logging.info("Adding service %s to stats", service)
            departed_details = services[service]
            count += 1
            scheduled_time = datetime.strptime(departed_details['scheduled'], "%H:%M")
            if departed_details['estimated'] == 'Cancelled':
                scheduled_time = datetime.strptime(departed_details['scheduled'], "%H:%M")
                logging.info('%s %s scheduled: %s cancelled', self.station_code, service, scheduled_time.strftime("%H:%M"))
                self.cancelled_count += 1
            elif departed_details['estimated'] == 'On time':
                logging.debug('%s %s:%s On Time!', self.station_code, service, scheduled_time.strftime("%H:%M"))
                self.on_time += 1
            elif departed_details['estimated'] == 'Delayed':
                logging.error('%s %s Delayed with no time - not added to stats!', self.station_code, service)
                logging.error(departed_details)
            else:
                departed_time = datetime.strptime(departed_details["estimated"], "%H:%M")
                logging.debug('%s %s scheduled: %s; departed:%s', self.station_code, service,
                              scheduled_time.strftime("%H:%M"), departed_time.strftime("%H:%M"))
                logging.debug('%d seconds late', (departed_time-scheduled_time).total_seconds() / 60)
                seconds_late = (departed_time-scheduled_time).total_seconds()
                if seconds_late <= 0:
                    seconds_late += SECONDS_IN_DAY
                    logging.warning("%s: %s departed %d seconds early", self.station_code, service, seconds_late)

                self.departure_delays.append(seconds_late / 60)

        if count > 0:
            logging.info("%d departed trains processed at %s", count, self.station_code)
