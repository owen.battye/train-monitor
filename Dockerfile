FROM python:3-slim-bookworm AS base
WORKDIR /app
ADD requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt

FROM base AS trainmonitor
ADD *.py .
ADD templates ./templates

FROM base AS checker
CMD ["pip3", "list", "--outdated"]
