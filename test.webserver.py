#!/usr/bin/python3

import unittest
import webserver

class WebServer_Test(unittest.TestCase):

    def test_check_stat_generation_from_data(self):
        data = ['MCO', 25, 2024, 257, 88, 183, -0.4754098, 108, 10.787037037037037036, 4, 36.5]
        results = webserver.single_station_week(data)
        self.assertEqual(results['totals']['total'], 528)
        self.assertEqual(results['totals']['cancelled_count'], 88)
        self.assertEqual(results['totals']['on_time'], 257)

        self.assertEqual(results['delays']['all']['train_count'], 183)
        self.assertEqual(results['delays']['without_minor']['train_count'], 108)
        self.assertEqual(results['delays']['major']['train_count'], 4)

        self.assertAlmostEqual(results['accuracy']['all'], 48.67424242)
        self.assertAlmostEqual(results['accuracy']['without_minor'], 62.8787878787)


if __name__ == '__main__':
    unittest.main()
