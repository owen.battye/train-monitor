#!/bin/bash -e

IMAGE_NAME=bensonscave/trainmonitor:latest

docker build --pull --target trainmonitor -t ${IMAGE_NAME} . 

docker push ${IMAGE_NAME}