#!/usr/bin/python3

import logging
import pickle
from zeep import Client, Settings, xsd
from zeep.plugins import HistoryPlugin

LDB_TOKEN="51724cb2-bf0d-464d-a195-66fb5ef08757"
WSDL = 'http://lite.realtime.nationalrail.co.uk/OpenLDBWS/wsdl.aspx?ver=2021-11-01'

CLIENT = None
HEADER_VALUE = None
MAJOR_DELAY_TIME = 15
MINOR_DELAY_TIME = 3

def open_client():
    global CLIENT, HEADER_VALUE

    if LDB_TOKEN == '':
        logging.error("Please configure your OpenLDBWS token")
        raise Exception("Please configure your OpenLDBWS token")

    settings = Settings(strict=False)

    history = HistoryPlugin()
    CLIENT = Client(wsdl=WSDL, settings=settings, plugins=[history])
    header = xsd.Element(
        '{http://thalesgroup.com/RTTI/2013-11-28/Token/types}AccessToken',
        xsd.ComplexType([
            xsd.Element(
                '{http://thalesgroup.com/RTTI/2013-11-28/Token/types}TokenValue',
                xsd.String()),
        ])
    )
    HEADER_VALUE = header(TokenValue=LDB_TOKEN)


def get_location_name(station_code='URM', number_of_results=10):
    res = CLIENT.service.GetDepartureBoard(numRows=number_of_results, crs=station_code, _soapheaders=[HEADER_VALUE])
    return res.locationName

def get_departure_board(station_code='URM', number_of_results=10, print_board=False):
    res = CLIENT.service.GetDepartureBoard(numRows=number_of_results, crs=station_code, _soapheaders=[HEADER_VALUE])
    if (res is None or res.trainServices is None):
        logging.warning("Error no train services received @ %s", station_code)
        return []

    services = res.trainServices.service

    if print_board:
        logging.info("===============================================================================")
        logging.info("Current trains at %s", res.locationName)
        logging.info("===============================================================================")

        for service in services:
            logging.info("%s to %s - %s (%s)", service.std, service.destination.location[0].locationName, service.etd, service.serviceID)
            #logging.debug(service)

        logging.info("*******************************************************************************")
        if res.busServices is not None and len(res.busServices.service) > 0:
            logging.info("Buses")
            logging.info("Buses are excluded from stats")
            for service in res.busServices.service:
                logging.info("%s to %s - %s (%s)", service.std, service.destination.location[0].locationName, service.etd, service.serviceID)
            logging.info("*******************************************************************************")

    if (services is None or len(services) == 0):
        logging.warning("Error no services listed @ %s", station_code)
        return []

    logging.debug("%d services listed @ %s", len(services), station_code)

    return services


def get_service_details(service_id):
    try:
        res = CLIENT.service.GetServiceDetails(serviceID=service_id, _soapheaders=[HEADER_VALUE])
        return res

    except:
        logging.error("**** service not retrieved")

    return None

def save_to_file(service):
    with open(f'./data/{service}.pickle', 'wb') as file:
        pickle.dump(service, file)

open_client()

if __name__ == "__main__":
    logging.basicConfig(level="DEBUG", format="%(levelname)s: %(message)s", datefmt="[%X]")
    #print (get_service_details("3701775MNCRDGT"))
    #save_to_file(get_service_details("3219120URMSTON_"))
    #save_to_file(get_service_details("3216566URMSTON_"))
    get_departure_board(print_board=True)
