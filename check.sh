#!/bin/bash -e

IMAGE_NAME=bensonscave/trains/checker:latest

docker build --progress plain --pull --tag ${IMAGE_NAME} --target checker . 

docker run ${IMAGE_NAME}
