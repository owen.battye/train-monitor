#!/usr/bin/python3

import logging
from datetime import datetime
from flask import Flask, request, render_template, redirect, url_for
import database
import railclient

app = Flask(__name__)
logging.basicConfig(level="WARN", format="%(levelname)s: %(message)s", datefmt="[%X]")


def create_app():
    return app


@app.route('/')
@app.route('/rail')
def index():
    station_id = request.args.get('stationid', "MAN")

    return render_template('index.html', station_id=station_id, stations=stations(),
                           minor_delay=railclient.MINOR_DELAY_TIME, major_delay=railclient.MAJOR_DELAY_TIME)


@app.route('/history')
@app.route('/rail/history')
def historypage():
    station_id = request.args.get('stationid', "MAN")
    print (stations())

    return render_template('history.html', station_id=station_id, stations=stations())


@app.route('/api/history')
@app.route('/rail/api/history')
def history():
    station_id = request.args.get('stationid', "MAN")
    return_results = {}

    current_week = datetime.now().isocalendar().week
    current_year = datetime.now().year

    data = database.read_history(station_id)
    if data is not None:
        for row in data:
            add = True
            year = row[2]
            week_number = row[1]

            if year == current_year and week_number == current_week:
                add = False

            if add:
                year_results = return_results.get(year, {})
                year_results[week_number] = single_station_week(row)

                return_results[year] = year_results

    return {'station_id': station_id, "metrics": return_results }


@app.route('/api/current')
@app.route('/rail/api/current')
def current():
    station_id = request.args.get('stationid', "MAN")
    return_results = {}

    data = database.read_current(station_id)
    if data is not None and len(data) > 0:
        row = data[0]
        return_results = single_station_week(row)
    else:
        logging.error("No data read for %s", station_id)

    return {'station_id': station_id, "metrics": return_results }


@app.route('/api/stations')
@app.route('/rail/api/stations')
def stations():
    return_results = {}

    for station_details in database.get_stations():
        return_results[station_details[0]] = station_details[1]

    return return_results


@app.route('/api/station')
@app.route('/rail/api/station')
def station():
    station_id = request.args.get('stationid', "MAN")
    number_of_results = request.args.get('number', 5)

    rail_services = railclient.get_departure_board(station_id, number_of_results)
    return_results = []
    for service in rail_services:
        return_results.append({'destination': service['destination']['location'][0]['locationName'],
                               'scheduled': service['std'],
                               'estimated': service['etd'],
                               'operator': service['operatorCode']})

    return {'services': return_results }


def single_station_week(row):
    logging.debug(row)
    cancelled_count = row[4]
    total_delayed_count = row[5]
    delayed_without_minor_count = row[7]
    major_delayed_count = row[9]
    total_trains = row[3]+row[4]+total_delayed_count

    minor_delayed_count = total_delayed_count - delayed_without_minor_count
    delayed_count_without_major_and_minor = total_delayed_count - minor_delayed_count - major_delayed_count

    accuracy_all = 0
    accuracy_without_minor = 0
    if total_trains > 0:
        accuracy_all = row[3] / total_trains * 100
        accuracy_without_minor = (row[3]+minor_delayed_count) / total_trains * 100
    else:
        logging.error("No trains for this week")
        logging.error(row)

    return {'totals': {'on_time': row[3], 'cancelled_count': cancelled_count, 'total': total_trains},
            'plots' : [row[3], minor_delayed_count, delayed_count_without_major_and_minor, major_delayed_count, row[4]],
            'delays': {
                'all': {'train_count': total_delayed_count, 'average_delay': row[6]},
                'without_minor': {'train_count': delayed_without_minor_count, 'average_delay': row[8]},
                'major': {'train_count': major_delayed_count, 'average_delay': row[10]}
            },
            'accuracy':
            {'all': accuracy_all, 'without_minor': accuracy_without_minor}
           }


@app.route('/api/reset')
@app.route('/rail/api/reset')
def reset():
    database.set_reset()
    return redirect(url_for('index'))
