#!/bin/bash -e

docker compose up $* --build --pull always --remove-orphans webserver datacollector
