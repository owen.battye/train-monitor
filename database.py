#!/usr/bin/python
import logging
from datetime import datetime
import sqlite3
from sqlite3 import Error


logging.basicConfig(level="WARN", format="%(levelname)s: %(message)s", datefmt="[%X]")
CONNECTION = None


def create_connection():
    global CONNECTION
    try:
        if CONNECTION is None:
            CONNECTION = create_local_connection()
    except Error as e:
        logging.error("The error '%s' occurred creating connection", e)

    return CONNECTION


def create_local_connection(path="./data/railhistory.sqlite"):
    try:
        local_connection = sqlite3.connect(path)
        logging.info("Connection to SQLite DB successful")
        return local_connection
    except Error as e:
        logging.error("The error '%s' occurred creating connection", e)

    return None


def execute_query(query, connection=None):
    if connection is None:
        if CONNECTION is None:
            create_connection()
        connection=CONNECTION

    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
    except Error as e:
        logging.error("The error '%s' occurred executing commit: %s", e, query)


def read_query(query, connection=None):
    if connection is None:
        if CONNECTION is None:
            create_connection()
        connection=CONNECTION

    cursor = connection.cursor()
    try:
        cursor.execute(query)
        rows = cursor.fetchall()
        return rows

    except Error as e:
        logging.error("The error '%s' occurred", e)

    return None


def create_tables():
    create_history_table = """CREATE TABLE IF NOT EXISTS history (station_id TEXT NOT NULL,
    week INTEGER NOT NULL,
    year INTEGER NOT NULL,
    on_time INTEGER NOT NULL,
    cancelled_count INTEGER NOT NULL,
    trains_delayed INTEGER NOT NULL,
    average_delay INTEGER NOT NULL,
    trains_delayed_without_minor INTEGER NOT NULL,
    average_delay_without_minor INTEGER NOT NULL,
    trains_with_major_delays INTEGER NOT NULL,
    average_major_delay INTEGER NOT NULL,
    PRIMARY KEY(station_id,year,week)
    );
    """
    execute_query(create_history_table)

    create_name_table = """CREATE TABLE IF NOT EXISTS name (station_id TEXT NOT NULL,
    station_name TEXT,
    PRIMARY KEY(station_id)
    );
    """
    execute_query(create_name_table)

    create_reset_table = """CREATE TABLE IF NOT EXISTS reset (id INTEGER NOT NULL, reset TEXT NOT NULL,
    PRIMARY KEY(id));
    """
    execute_query(create_reset_table)
    logging.info("Tables created")


def save_station_name(station_code, station_name):
    logging.info("Save name of station %s: %s", station_code, station_name)
    add_station_record = f"""
    REPLACE INTO
    name (station_id, station_name)
    VALUES
    ('{station_code}', '{station_name}');
    """
    execute_query(add_station_record)


def get_stations(local_connection = None):
    if local_connection is None:
        local_connection = create_local_connection()

    get_stations_cmd = """
    SELECT * FROM name
    """
    return read_query(get_stations_cmd, local_connection)


def write_record(station_id, on_time, cancelled_count, trains_delayed, average_delay,
                 trains_delayed_without_minor, average_delay_without_minor,
                 trains_with_major_delays, average_major_delay, current_week=None, current_year=None):

    if current_week is None:
        current_week = datetime.now().isocalendar().week
    if current_year is None:
        current_year = datetime.now().year
    add_history_table_record = f"""
    REPLACE INTO
    history (station_id, week, year, on_time, cancelled_count, trains_delayed, average_delay, trains_delayed_without_minor, average_delay_without_minor, trains_with_major_delays, average_major_delay)
    VALUES
    ('{station_id}', {current_week}, {current_year}, {on_time}, {cancelled_count}, {trains_delayed}, {average_delay}, {trains_delayed_without_minor}, {average_delay_without_minor}, {trains_with_major_delays}, {average_major_delay});
    """
    execute_query(add_history_table_record)


def read_current(station_id="MAN", local_connection = None):
    if local_connection is None:
        local_connection = create_local_connection()

    current_week = datetime.now().isocalendar().week
    current_year = datetime.now().year

    query = f"SELECT * FROM history WHERE station_id == '{station_id}' AND year == {current_year} AND week == {current_week}"

    return read_query(query, local_connection)


def read_history(station_id = "MAN", local_connection = None):
    if local_connection is None:
        local_connection = create_local_connection()

    return read_query(f"SELECT * FROM history WHERE station_id == '{station_id}'", local_connection)


def reset_required():
    query = "SELECT reset FROM reset WHERE id == '0'"
    result = read_query(query)
    if len(result) == 0:
        return False

    return result[0][0] == 'True'


def set_reset(value=True, local_connection = None):
    if local_connection is None:
        local_connection = create_local_connection()

    add_reset_record = f"""
    REPLACE INTO
    reset (id, reset)
    VALUES
    (0, '{str(value)}');
    """
    execute_query(add_reset_record, local_connection)


create_tables()

if __name__ == '__main__':
    # print (get_stations())

    set_reset(False)
    print (reset_required())
