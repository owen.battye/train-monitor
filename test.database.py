#!/usr/bin/python3

import unittest
from datetime import datetime, timedelta
import database
import random

class DatabaseTest(unittest.TestCase):

    def test_check_station_names(self):
        database.save_station_name('MAN', "Manc Picc")
        database.save_station_name('DGT', "Manc Deansgt")
        database.save_station_name('MCO', "Manc Ox Rd")
        database.save_station_name('URM', "Urms")

        stations = database.get_stations()
        self.assertEqual(len(stations), 4)


    def test_check_history_for_urmston(self):
#        database.write_record(self.station_code, self.on_time, self.cancelled_count,
#                              len(self.departure_delays), trains_delayed_mean,
#                              number_trains_longer_delays, longer_delays_mean,
#                              number_trains_major_delays, major_delays_mean)


        start_day = datetime.now()
        for _ in range(5):
            trains_on_time = random.randrange(100,150)
            trains_cancelled = random.randrange(1,10)
            trains_delayed = random.randrange(1,5)
            trains_major_non_minor_delays = random.randrange(2,5)
            trains_major_delayed = random.randrange(1,trains_major_non_minor_delays)
            database.write_record('MCO', trains_on_time, trains_cancelled,
                                  trains_delayed, 2.4,
                                  trains_major_non_minor_delays, 3.4,
                                  trains_major_delayed, 10.5,
                                  start_day.isocalendar().week, start_day.year)
            start_day -= timedelta(days=7)



if __name__ == '__main__':
    unittest.main()
