#!/usr/bin/python3

import time
from datetime import datetime
import logging
from station import Station
import database

stations = [Station('MAN'), Station("URM"), Station("MCO"), Station("DGT"), Station("MCV")]
logging.basicConfig(level="DEBUG", format="%(levelname)s: %(message)s", datefmt="[%X]")


def pause(refresh_interval = 45):
    while refresh_interval > 0:
        if refresh_interval % 5 == 0:
            logging.info("Refresh in %d seconds", refresh_interval)
        time.sleep(1)
        refresh_interval -= 1

last_cleared_weekday = datetime.now().date().isocalendar().weekday

while True:
    if database.reset_required():
        database.set_reset(False)
        for station in stations:
            station.clear()

    CLEAR_TRAINS = False
    today_weekday = datetime.now().date().isocalendar().weekday
    if today_weekday < last_cleared_weekday:
        last_cleared_weekday = today_weekday
        CLEAR_TRAINS = True

    for station in stations:
        station.find_departed_trains()
        station.print_stats()
        station.save_stats()
        if CLEAR_TRAINS:
            print ("Clearing station")
            station.clear()

    pause()
